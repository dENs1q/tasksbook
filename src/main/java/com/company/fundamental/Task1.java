package com.company.fundamental;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        String s = "";
        if (sc.hasNextInt()) {
            a = sc.nextInt();
        } else if (sc.hasNextLine()) {
            s = sc.nextLine();
        }
        System.out.println("Binary " + Integer.toBinaryString(a));
        System.out.println("Octal " + Integer.toOctalString(a));
        System.out.println("Hex " + Integer.toHexString(a));
        System.out.println("parseInt " + Integer.parseInt(s, 16));

//        System.out.println("My conversion " + NumeralSystem.conversion(a, 16));
    }
}
