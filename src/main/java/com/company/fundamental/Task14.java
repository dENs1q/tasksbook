package com.company.fundamental;

import com.company.fundamental.utils.MagicSquare;

import java.util.ArrayList;
import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<Integer> ints = new ArrayList<>();
        String input;
        int current;
        do {
            System.out.print("Enter int ");
            input = in.nextLine();
            if (!input.equals("")) {
                current = Integer.parseInt(input);
                ints.add(current);
            }
        } while (!input.equals(""));

        int numInputs = ints.size();
        int square = (int) Math.sqrt(numInputs);

        int[][] intSquare = new int[square][square];
        int x = 0;
        while (x < numInputs) {
            for (int y = 0; y < square; ++y) {
                for (int z = 0; z < square; ++z) {
                    intSquare[y][z] = ints.get(x);
                    ++x;
                }
            }
        }
        if (MagicSquare.isMagicSquare(intSquare)) {
            System.out.println("Entered magic square");
        } else {
            System.out.println("Not enter magic square");
        }

    }
}
