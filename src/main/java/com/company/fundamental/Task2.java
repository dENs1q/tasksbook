package com.company.fundamental;

import com.company.fundamental.utils.Normalize;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        System.out.println("Write integer angle");
        if (sc.hasNextInt()) {
            a = sc.nextInt();
        }else {
            System.out.println("It`s not number");
        }
        System.out.println("Normalize angle = " + Normalize.normalize(a));
    }
}
