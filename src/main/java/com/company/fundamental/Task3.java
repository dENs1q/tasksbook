package com.company.fundamental;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        int b = 0;
        int c = 0;
        System.out.println("Write three number");
        if (sc.hasNextInt()) {
            a = sc.nextInt();
            b = sc.nextInt();
            c = sc.nextInt();
        } else {
            System.out.println("It`s not number");
        }
        System.out.println(a > b ? a : b > c ? b : c);
        System.out.println(Math.max(a, Math.max(b, c)));
    }
}