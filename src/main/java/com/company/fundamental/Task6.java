package com.company.fundamental;

import com.company.fundamental.utils.Factorial;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        System.out.println("Write integer");
        if (sc.hasNextInt()) {
            a = sc.nextInt();
        }
        System.out.println("Factorial = " + Factorial.getFactorial(a));
    }
}
