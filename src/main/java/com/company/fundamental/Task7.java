package com.company.fundamental;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write two integer ");
        int a = 0;
        int b = 0;
        if (sc.hasNextInt()) {
            a = sc.nextInt();
            b = sc.nextInt();
        } else {
            System.out.println("It`s not number");
        }
        System.out.println("Divide: " + Integer.divideUnsigned(a, b));
        System.out.println("Remainder of the division: " + Integer.remainderUnsigned(a, b));

//        String aStr = Integer.toUnsignedString(a);
//        String bStr = Integer.toUnsignedString(b);

        System.out.println("Add: " + Integer.toUnsignedString(a + b));
        System.out.println("Difference: " + Integer.toUnsignedString(a - b));
        System.out.println("Multiply: " + Integer.toUnsignedString(a * b));
        System.out.println("Divide: " + Integer.toUnsignedString(a / b));


    }
}
