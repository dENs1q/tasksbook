package com.company.fundamental;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "";
        if (sc.hasNextLine()) {
            str = sc.nextLine();
        } else {
            System.out.println("It`s not string");
        }
        String[] arrStr = str.split(" ");
        System.out.println(Arrays.toString(arrStr));
    }
}
