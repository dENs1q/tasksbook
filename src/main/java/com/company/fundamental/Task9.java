package com.company.fundamental;

public class Task9 {
    public static void main(String[] args) {
        String s = "abc";
        String t = new String("abc");
        System.out.println(s.equals(t));
        System.out.println(s == t);
    }
}
