package com.company.fundamental.utils;

import java.math.BigInteger;

public class Factorial {
    private Factorial() {
    }

    public static BigInteger getFactorial(int i) {
        if (i <= 1) {
            return BigInteger.valueOf(1);
        } else {
            return BigInteger.valueOf(i).multiply(getFactorial(i - 1));
        }
    }
}
