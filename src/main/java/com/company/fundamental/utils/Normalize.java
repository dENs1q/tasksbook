package com.company.fundamental.utils;

public class Normalize {

    // using "%"
//    public static int normalize(int angle) {
//        angle = angle % 360;
//        if (angle < 0) {
//            angle = angle + 360;
//        }
//        return angle;
//    }

    // using floorMod
    public static int normalize(int angle) {
        angle = Math.floorMod(angle, 360);
        if (angle < 0) {
            angle = angle + 360;
        }
        return angle;
    }
}
