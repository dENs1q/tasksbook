package com.company.fundamental.utils;

public class NumeralSystem {

    private NumeralSystem() {
    }

    public static String conversion(int number, int numeral) {
        String result = "";
        while (number > 0) {
            String remnant;
            remnant = String.valueOf(number % numeral);
            number = (int) Math.floor(number / numeral);
            if (numeral == 16) {
                switch (remnant) {
                    case "10":
                        remnant = "a";
                        break;
                    case "11":
                        remnant = "b";
                        break;
                    case "12":
                        remnant = "c";
                        break;
                    case "13":
                        remnant = "d";
                        break;
                    case "14":
                        remnant = "e";
                        break;
                    case "15":
                        remnant = "f";
                        break;
                }
            }
            result += remnant;
        }
        return new StringBuilder(result).reverse().toString();
    }
}
