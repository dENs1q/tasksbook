package com.company.fundamental.utils;


import java.util.ArrayList;
import java.util.Collections;

public class RandomNumber {

    public static ArrayList<Integer> getRandomNumber() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 1; i < 50; i++) {
            arrayList.add(i);
        }
        int randomIndex;
        int randomNumber;
        for (int i = 0; i < 6; i++) {
            randomIndex = (int) (Math.random() * arrayList.size());
            randomNumber = arrayList.get(randomIndex);
            result.add(randomNumber);
            arrayList.remove(randomIndex);
        }
        Collections.sort(result);
        return result;
    }
}
