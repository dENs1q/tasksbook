package com.company.fundamental.utils;

public class RandomString {
    public static String getRandomString(int n) {

        String allSimbol = "abcdefghijklmnopqrstuvxyz"
                + "0123456789"
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int) (allSimbol.length() * Math.random());
            sb.append(allSimbol.charAt(index));
        }
        return sb.toString();
    }
}