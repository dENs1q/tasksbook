package com.company.inheritanceandreflection;

public enum Colors {
    BLACK,
    RED,
    BLUE,
    GREEN,
    CYAN,
    MAGENTA,
    YELLOW,
    WHITE;

    public static Colors getRed(){
        return RED;
    }
    public static Colors getGreen(){
        return GREEN;
    }
    public static Colors getBlue(){
        return BLUE;
    }

}
