package com.company.inheritanceandreflection;

import com.company.inheritanceandreflection.shape.Point;
import com.company.inheritanceandreflection.shape.Shape;

public class Task1to5 {
    public static void main(String[] args) {
        Point.LabeledPoint onePoint = new Point.LabeledPoint("onePoint", 10, 15);
        System.out.println("onePoint.getY(), getY() = " + onePoint.getX() + ", " + onePoint.getY());

        Shape.Circle circle = new Shape.Circle(new Point(10,10),3);
        Shape.Rectangle rectangle = new Shape.Rectangle(new Point(0,0),2,3);
        Shape.Line line = new Shape.Line(new Point(0,0), new Point(10,10));
        System.out.println("circle = " + circle.getCenter());
        System.out.println("rectangle = " + rectangle.getCenter());
        System.out.println("line.getCenter() = " + line.getCenter());
    }
}
