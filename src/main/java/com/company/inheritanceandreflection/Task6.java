package com.company.inheritanceandreflection;

import com.company.inheritanceandreflection.utils.DiscountedItem;
import com.company.inheritanceandreflection.utils.Item;

public class Task6 {
    public static void main(String[] args) {
        Item x = new Item("description", 10);
        Item y = new Item("description", 10);
        DiscountedItem z = new DiscountedItem("description", 10,5);
//        something went wrong
        System.out.println("x.equals(y) = " + x.equals(y));
        System.out.println("y.equals(z) = " + y.equals(z));
        System.out.println("x.equals(z) = " + x.equals(z));
    }
}
