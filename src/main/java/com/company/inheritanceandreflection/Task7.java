package com.company.inheritanceandreflection;


public class Task7 {
    public static void main(String[] args) {
        System.out.println("Colors.BLUE = " + Colors.BLUE);
        Colors red = Colors.getRed();
        System.out.println("Colors.RED = " + red);
    }
}
