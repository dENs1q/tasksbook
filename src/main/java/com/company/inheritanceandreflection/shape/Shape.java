package com.company.inheritanceandreflection.shape;

public abstract class Shape {
    private Point point;

    public Shape(Point point) {
        this.point = point;
    }

    public void moveBy(double dx, double dy) {
        this.point.x += dx;
        this.point.y += dy;
    }

    public abstract Point getCenter();

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static class Circle extends Shape {
        private double radius;

        public Circle(Point center, double radius) {
            super(center);
            this.radius = radius;
        }

        @Override
        public Point getCenter() {
            return super.point;
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

    public static class Rectangle extends Shape {
        private double width;
        private double height;

        public Rectangle(Point topLeft, double width, double height) {
            super(topLeft);
            this.width = width;
            this.height = height;
        }

        @Override
        public Point getCenter() {
            double x1 = super.point.getX();
            double x2 = super.point.getX() + width;
            double y1 = super.point.getY();
            double y2 = super.point.getY() + height;
            return new Point(
                    ((x1 + (x2 - x1)) / 2),
                    ((y1 + (y2 - y1)) / 2)
            );
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

    public static class Line extends Shape {
        private Point to;

        public Line(Point from, Point to) {
            super(from);
            this.to = to;
        }

        @Override
        public Point getCenter() {
            double x1 = super.point.getX();
            double x2 = to.getX();
            double y1 = super.point.getY();
            double y2 = to.getY();
            return new Point((x1 + x2) / 2, (y1 + y2) / 2);
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }
}
