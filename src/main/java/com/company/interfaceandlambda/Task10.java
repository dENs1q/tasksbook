package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.ThreadTask;

public class Task10 {
    public static void main(String[] args) {
        ThreadTask.runInOrder();
        ThreadTask.runTogether();
    }
}
