package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.FileList;

import java.io.File;
import java.util.Arrays;


public class Task11and12 {
    public static void main(String[] args) {
        FileList list = new FileList(new File("."));
        File[] strings1 = list.listFiles(File::isDirectory);
        System.out.println("strings1 = " + Arrays.toString(strings1));
        File[] strings2 = list.list((dir, name) -> name.toLowerCase().endsWith(".txt"));
        System.out.println("Arrays.toString(strings2) = " + Arrays.toString(strings2));
    }
}
