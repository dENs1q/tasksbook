package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.FileList;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Task13 {
    public static void main(String[] args) {
        FileList list = new FileList(new File("."));
        List<File> files = Arrays.asList(list.listFiles(pathname -> true));
        files.sort((o1, o2) -> {
            if (o1.isDirectory() && !o2.isDirectory()) {
                return -1;
            }
            if (o1.isDirectory() && o2.isDirectory() || !o1.isDirectory() && !o2.isDirectory()) {
                return o1.getName().compareTo(o2.getName());
            }
            return 1;
        });
        System.out.println("files = " + files);
    }
}
