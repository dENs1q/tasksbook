package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.Employee;

public class Task1and2 {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Anatoly", 1200);
        Employee employee2 = new Employee("Vadim", 1300);
        Employee employee3 = new Employee("Kostay", 1500);
        Employee employee4 = new Employee("Anna", 1400);

        double average = employee1.average(new Employee[]{employee1, employee2, employee4});
        String largest = employee1.largest(new Employee[]{employee1, employee2, employee3, employee4});
        System.out.println(average);
        System.out.println(largest);
    }
}
