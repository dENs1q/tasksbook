package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.MySort;

import java.util.ArrayList;
import java.util.Arrays;

public class Task8 {
    public static void main(String[] args) {
        MySort myClass = new MySort();
        ArrayList<String> strings = new ArrayList<>(Arrays.asList("x", "a", "z", "d", "x", "a", "z", "d", "x", "a", "z", "d", "x", "a", "z"));
        System.out.println("strings = " + strings);
        long start = System.nanoTime();
        myClass.luckySort(strings, String::compareTo);
        long finish = System.nanoTime();
        System.out.println("strings = " + strings);
        System.out.println("(finish - start) / 1000000 = " + (finish - start) / 1000000);
    }
}
