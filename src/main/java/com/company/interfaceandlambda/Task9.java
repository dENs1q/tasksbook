package com.company.interfaceandlambda;

import com.company.interfaceandlambda.implandutils.Greeter;

public class Task9 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Greeter(10, "Masha"));
        Thread thread2 = new Thread(new Greeter(5, "World"));

        thread1.start();
        thread2.start();
    }
}
