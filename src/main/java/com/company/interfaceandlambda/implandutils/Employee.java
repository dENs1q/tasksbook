package com.company.interfaceandlambda.implandutils;

public class Employee implements Measurable {
    private String firstName;
    private double salary;

    public Employee(String firstName, double salary) {
        this.firstName = firstName;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public double getMeasure() {
        return this.salary;
    }

    public double average(Measurable[] objects) {
        double sum = 0;
        if (objects != null
                && objects.length != 0) {
            for (Measurable o : objects) {
                sum += o.getMeasure();
            }
            return sum / objects.length;
        }
        return sum;
    }

    @Override
    public String largest(Measurable[] objects) {
        double max = 0;
        String name = "";
        if (objects != null
                && objects.length != 0) {
            for (Measurable o : objects) {
                if (o.getMeasure() > max) {
                    max = o.getMeasure();
                    Employee employee = (Employee) o;
                    name = employee.getFirstName();
                }
            }
            return name;
        }
        return name;
    }
}

