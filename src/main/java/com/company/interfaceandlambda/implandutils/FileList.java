package com.company.interfaceandlambda.implandutils;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

public class FileList {
    private File dir;

    public FileList(File path) {
        this.dir = path;
    }

    public File[] listFiles(FileFilter filter) {
        return dir.listFiles(filter);
    }

    public File[] list(FilenameFilter filter) {
        return dir.listFiles(filter);
    }
}
