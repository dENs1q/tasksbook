package com.company.interfaceandlambda.implandutils;

public class IntSequence {

    public static InnerSequence of(int... args){
        return new InnerSequence(){
            @Override
            public int[] getSequence() {
                return args;
            }
        };
    }

    public static int[] constant(int num){
        return null;
    }

    private static abstract class InnerSequence{
        public abstract int[] getSequence();
    }
}
