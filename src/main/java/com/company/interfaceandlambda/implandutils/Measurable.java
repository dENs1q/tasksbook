package com.company.interfaceandlambda.implandutils;

public interface Measurable {
    double getMeasure();
    String largest(Measurable[] objects);
}
