package com.company.interfaceandlambda.implandutils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MySort {

    public void luckySort(ArrayList<String> strings, Comparator<String> comp) {
        strings.sort(comp);

        boolean isOrder = false;
        while (!isOrder) {
            Collections.shuffle(strings);
            for (int i = 0; i < strings.size() - 1; i++) {
                int compare = comp.compare(strings.get(i), strings.get(i + 1));
                if (compare > 0) {
                    isOrder = false;
                    break;
                }
                isOrder = true;
            }
        }
    }
}
