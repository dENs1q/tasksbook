package com.company.interfaceandlambda.implandutils;

public class ThreadTask {
    public static void runTogether(Runnable... tasks) {
        for (Runnable task : tasks) {
            new Thread(task).start();
        }
    }

    public static void runInOrder(Runnable... tasks) {
        Thread thread = null;
        for (Runnable task : tasks) {
            thread = new Thread(task);
            thread.start();
            try {
                thread.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        if (thread != null) {
            thread.notifyAll();
        }
    }
}
