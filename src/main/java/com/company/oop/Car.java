package com.company.oop;

public class Car {
    private double gas = 0;
    private double consumption; // liters / 100 km
    private double distance = 0;

    public Car() {
    }

    public double getGas() {
        return gas;
    }

    public double getDistance() {
        return distance;
    }

    public Car(double consumption) {
        this.consumption = consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public void drive(double distance) {
        double consumptionOneKh = this.consumption / 100;
        double needGas = consumptionOneKh * distance;
        if (needGas < this.gas) {
            this.gas -= needGas;
            this.distance += distance;
        }else{
            System.out.println("Not enough fuel");
        }
    }

    public void fill(double quantity) {
        this.gas += quantity;
    }

    @Override
    public String toString() {
        return "Car{" +
                "consumption = " + consumption +
                ", fuel level = " + gas +
                ", distance covered = " + distance +
                '}';
    }
}
