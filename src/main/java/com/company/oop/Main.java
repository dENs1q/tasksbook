package com.company.oop;

public class Main {
    public static void main(String[] args) {
        Point point = new Point(3, 4);
        System.out.println(point.translate(1,3).scale(0.5));
//////////////////////////////////////////////////////////////////////////////
        // class car is mutable as we can refuel the car
        Car car = new Car(10);
        System.out.println(car);
        car.fill(10);
        System.out.println(car);
        car.drive(50);
        car.drive(60);
        System.out.println(car);
    }
}
