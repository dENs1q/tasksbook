package com.company.oop;

public final class Point {
    private double x = 0;
    private double y = 0;

    public Point() {
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point translate(double x, double y) {
        Point newPoint = new Point(getX() + x, getY() + y);
        return newPoint;
    }

    public Point scale(double multiplier) {
        Point newPoint = new Point(getX() * multiplier, getY() * multiplier);
        return newPoint;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
